const gulp = require("gulp");
const sass = require("gulp-sass");
const imagemin = require('gulp-imagemin');
const minify = require('gulp-js-minify');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();



gulp.task('js', function (done) {
    gulp.src('src/js/*.js')
        // .pipe(minify())
        .pipe(gulp.dest('dist/'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('img', function (done) {
    gulp.src('src/img/**/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('css', function (done) {
    gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
    done()
});


gulp.task('build', gulp.parallel('js', 'img', 'css'));

const watch = () => {
    browserSync.init({server: {baseDir: './'}});
    gulp.watch(['src/js/*.js', 'src/scss/*.scss','src/img/**/*.*'], gulp.series('js', 'css', 'img'));
};

gulp.task('watch', gulp.series('build', watch));
